import React, { useState, useEffect, FormEvent } from 'react';
import { Paginated } from '@feathersjs/feathers';
import client from './feathers';

interface Patient {
  // add the required properties here

  id: number
};

type PatientFunction = (g: Patient) => void;

interface AllPatientFuntions {
  addPatient: PatientFunction,
  removePatient: PatientFunction
};

let patientFuncs: AllPatientFuntions = {
  addPatient: (c: Patient) => {},
  removePatient: (c: Patient) => {}
};

const patientsService = client.service('patients');

patientsService.on( 'created', (newPatient: Patient) => {
  patientFuncs.addPatient( newPatient );
});

patientsService.on( 'removed', (oldPatient: Patient) => {
  patientFuncs.removePatient( oldPatient );
});

function Patients() {
  // you'll have to add some additional state varialbes here
  // for each of the form fields

  const [allPatients, setAllPatients] = useState<Array<Patient>>([]);


  const handleDelete = (id: number) => {
      patientsService.remove( id );
  }

  const patientRows = allPatients.map( (patient: Patient ) =>
    <tr key={patient.id}>
      <td>{patient.id}</td>
      {/* you'll have to define the additional columns here */}
      <td><button onClick={() => handleDelete(patient.id)} type="button" className="btn btn-danger">Delete</button></td>
    </tr>
  );

  useEffect(() => {
    function addPatientX( newPatient: Patient ) {
      setAllPatients( [...allPatients, newPatient] );
    }

    function removePatientX( oldPatient: Patient ) {
      const newPatients = allPatients.filter((ipatient,index,arr) => {
        return ipatient.id !== oldPatient.id;
      });
      setAllPatients( newPatients );
    }

    patientFuncs.addPatient = addPatientX;
    patientFuncs.removePatient = removePatientX;
  });

  useEffect(() => {
    patientsService
    .find()
    .then( (patientPage: Paginated<Patient>) => setAllPatients( patientPage.data ))
    .catch( (err: any) => {
      console.log( "problem finding patients.");
    });
  }, []);

  const handleSubmit = (e: FormEvent) => {
      e.preventDefault();
      const element = e.currentTarget as HTMLFormElement;
      if ( element.checkValidity() ) {
        element.classList.remove('was-validated');
        patientsService
        .create({})
        .then( (patient: Patient) => {
          // successfully created patient
        })
        .catch( (err: any) => {
          // failed to create patient

        });
      } else {
        element.classList.add('was-validated');
      }
  }

  return (
    <div>
      <div className="py-5 text-center">
        <h2>Patients</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={handleSubmit} noValidate>
            <div className="row">
              {/* you'll have to add additional form fields here */}

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add patient</button>
          </form>
        </div>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            {/* you'll have to add additional columns here */}
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>

          {patientRows}

        </tbody>
      </table>

    </div>
  );
}

export default Patients;
